import uuid

def strong_em_last_item(text):
    temp = str(uuid.uuid4())

    text = text.replace('\,', temp)

    text_parts = text.split(',')

    text_parts_length = len(text_parts)

    text_parts[text_parts_length - 1] = ''.join([
        '<strong><em>',
        text_parts[text_parts_length - 1],
        '</em></strong>'
    ])

    return ','.join(text_parts).replace(temp, ',')
