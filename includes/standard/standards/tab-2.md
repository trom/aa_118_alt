## {{ standards-distinction-heading[heading] <span>02</span> Distinction }}

{{ standards-distinction Ads should always be recognizable as ads and distinguishable from other content. Ads should clearly be marked with the word “advertisement” or its equivalent. }}
