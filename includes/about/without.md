## {{ without-heading[heading] Without <span class="group">Acceptable Ads</span> }} {: .h1 }

{{ without-1 Overwhelming amounts of poor-quality online advertising exists. This disrupts the user experience and leads to huge amounts of users installing ad blockers. }}

{{ without-2 Users are frustrated, advertisers miss out on an important audience, and publishers miss out on advertising revenue. Everyone loses. }}
